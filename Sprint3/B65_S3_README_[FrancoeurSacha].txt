Nom : Sacha Francoeur
Projet : Conju
Contenu : README.txt


########################### INSTALLATION POUR UN UTILISATEUR NON-TECHNIQUE ##########################################

INSTALLATION DE PYCHARM
1 - Ouvrir le navigateur de votre choix et aller sur www.google.ca

2 - Tapez PyCharm sur la barre de recherche Google.

3 - Sélectionner le site de JetBrain qui développe l'IDE Python, PyCharm.

4 - Cliquer sur le bouton bleu, Télécharger, celui-ci défilera plus bas.

5 - Choisissez la version approprié pour votre système d'exploitation.

6 - Cliquer sur le bouton noire, contenant Télécharger, car c'est la version Community que nous cherchons.

7 - Attendez que l'executable finissent de télécharger.

8 - Cliquer sur l'exécutable, pesez sur Next, puis sur Next, choisissez 64-bit Launcher, puis encore sur Next et puis sur Install.
Attendez la fin de l'installation, pour finalement peser sur Finish.

9 - PyCharm est maintenant installé.

----------------------------------------------------------------------------------------------------

INSTALLATION DE MYSQL

1 - Ouvrir le navigateur de votre choix et aller sur www.google.ca

2 - Tapez mysql sur la barre de recherche Google.

3 - Dans la premiere requête de Google, vous devriez voir un lien indiquant MySQL Community Downloads, cliquer sur celui-ci.

4 - Sur la nouvelle page, cliquer sur le lien indiquant MySQL Community Server.

5 - Installer la version du système d'exploitation de votre choix, en allant cliquer sur le Download.
Choisir celui avec le MSI, cliquer sur "Go to Download Page".

6 - Cliquer sur la version ayant le plus de téléchargement, donc la deuxième, en pesant sur le bouton Download.

7 - Vous serez redirigez sur un autre page, cliquer sur le lien "No thanks, just start my download".
Attendre la fin du téléchargement.

8 - Cliquer sur l'exécutable et sur le bouton Exécuter

9 - Choississez la version "Developer Default" et cliquer sur le bouton "Next".

10 - Cliquer sur "Execute", et attendez la fin de ces installations, puis pesez sur Next, puis encore sur Next.

11 - Laisser la configuration comme elle est, puis cliquer sur "Next".

12 - Même chose et peser encore sur "Next", puis encore la même chose et cliquer sur "Next".

13 - Choisir le mot de passe pour utilisateur "Root". Ici, ça reste votre choix.

14 - Cliquer sur "Next" et puis encore sur "Next", puis sur "Execute".

15 - Quand tout ceci est terminer cliquer sur "Finish", puis sur "Next", puis sur "Finish", puis sur "Next".

16 - Rentrer votre mot de passe , cliquer sur "Check", puis sur "Next".

17 - Cliquer sur "Execute", lorsque c'est terminé, cliquer sur "Finish", puis sur "Next".

18 - Puis recliquez sur "Finish".

L'installation de MySQL est enfin terminée.

-------------------------------------------------------------------------------------------------------------

INSTALLATION DE GIT

1 - Ouvrir le navigateur de votre choix et aller sur www.google.ca

2- Tapez git sur votre barre de recherche.

3- Choississez la première option qui est le site "git-scm.com"

4- Cliquer sur le bouton gris "Download for ****", attendez la fin du téléchargement.

5 - Cliquer sur l'exécutable, puis cliquer sur Exécuter, puis sur "Next", encore sur "Next", encore sur "Next".
Encore sur "Next", puis encore sur "Next", encore sur "Next", puis toujours "Next", encore "Next", encore "Next".
Toujours "Next", puis sur "Install", puis sur "Finish".

----------------------------------------------------------------------------------------------------------------

INSTALLER PYTHON

1 - Ouvrir le navigateur de votre choix et aller sur www.google.ca

2 - Tapez python sur la barre de recherche.

3 - Cliquer sur le premier lien.

4 - Cliquer sur Downloads.

5 - Cliquer sur le bouton jaune "Download Python ..."
	Attendre la fin du téléchargement

6 - Cliquer sur l'exécutable, puis sur "Install Now"

7 - Lorsque le tout est terminé, cliqué sur "Close"

---------------------------------------------------------------------------------------------------------------------

ALLER CHERCHER LE PROJET GIT

1 - Ouvrir le navigateur de votre choix et aller sur www.google.ca

2 -  Rentrer ce lien pour accéder au GIT : "https://FrancoeurS@bitbucket.org/FrancoeurS/b65_sacha_francoeur.git"

3 - Cliquer sur le bouton "Clone" en haut à droite", puis copier l'info présente.

4 - Faite un clic droit sur le bureau, et ouvrer Git Bash.

5 - Coller le contenu en faisant un clic droit, sur "Paste", puis sur Enter.

Le contenu du git devrait être alors sur votre ordinateur.

---------------------------------------------------------------------------------------------------------------------

POUR SE CONNECTER À MYSQL ET CRÉÉ LES TABLES

1 - Ouvrez MySQL WorkBench.

2 - Cliquer sur le "Local Instance", puis entrer votre mot de passe.

3 - Aller dans la Query1 et puis rentrer ses trois lignes de commande :

	CREATE USER 'conju'@'localhost' IDENTIFIED BY 'conjuGate';
	GRANT ALL privileges ON CONJU.* to 'conju'@'localhost';
	ALTER USER 'conju'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';

4 - Aller dans File et cliquer sur "Open SQL Script"

5 - Choississez le fichier SQL, table.sql et faire ouvrir.

6 - Cliquer sur l'icône avec l'éclair sans curseur pour faire rouler le script de toute la page.

7 - Répétez l'étape 5, mais cette fois-ci avec verbe.sql.

8 - Prener les deux premières et les exécuter avec l'icône de l'éclair avec un curseur, une à la fois.

9 - Répétez l'étape 5, mais cette fois-ci avec present.sql.

10 - Répétez l'étape 8.

11 - Répétez l'étape 5, mais cette fois-ci avec imparfait.sql.

12 - Répétez l'étape 8.

13 - Répétez l'étape 5, mais cette fois-ci avec phrase_present_facile.sql.

14 - Répétez l'étape 8.

15 - Répétez l'étape 5, mais cette fois-ci avec phrase_imparfait_facile.sql.

L'insertion des données et la création de tables MySQL fonctionne, maintenant.

-------------------------------------------------------------------------------------------------------------------

POUR FAIRE FONCTIONNER LE PROJET

1 - Créer vous un dossier dans l'endroit que vous souhaitez.

2 - Cliquer sur le raccourci PyCharm, et cliquer "I confirm that ...", puis cliquer sur "Continue"

3 - Cliquer sur le cas que vous souhaitez.

4 - La fenêtre PyCharm s'affiche, cliquez sur "Create New Project", puis trouver le dossier correspondant à celui que vous vener de crée.

4 - Cliquer sur la flèche contenant "Projet Interpreter: New Virtualenv environment"

5 - Dans Location, choississez le chemin que vous avez créer pour le dossier

6 - Assurer que l'interpréteur Python est le bon.

7 - Cliquer sur "Create".

8 - Copier les fichiers Python provenant du dossier GIT et copier les dans le dossier créée.

9 - Aller sur le fichier main.py, en voyant celui-ci vous devriez voir PyQt5 en rouge.

10 - Vous allez voir "Install package PyQt5", cliquer dessus.
	Attendez la fin de l'installation.

11 - Aller en haut à gauche dans "Files", puis aller dans "Settings".

12 - Cliquer sur "Projet: Nom de votre Projet", puis "Project Interpreter".

13 - Cliquer ensuite sur l'icône avec un +.

14 - Inscriver mysql-connector dans la barre de recherche et faite "Install Package" sur celui-ci".

15 - Faite un clic droit dans le main, pis faite Run "main". (Ou Ctrl+Maj+F10)


MAINTENANT, le tout devrait fonctionné.




####################### INSTALLATION POUR UN UTILISATEUR TECHNIQUE #############################################

INSTALLATION DE PYCHARM
1 - Rechercher pour PyCharm dans Google.

2 - En allant sur Télécharger, choisissez la version approprié
pour votre système d'exploitation et téléchargez la version Community.

3- Ouvrir l'exécutable et cliquer sur Next, jusqu'au bouton Install.
 (Vous pouvez choisir si vous souhaitez avoir un raccourci Bureau)

4 - Lorsque le tout est terminé, PyCharm est installé.

----------------------------------------------------------------------------------------------------------------

INSTALLATION DE MYSQL

1 - Rechercher MySQL et allez sur le lien de MySQL Community Download.

2 - Cliquer sur le lien MySQL Community Server et choisir par rapport au système d'exploitation convenue.

3 - Choisissez le MSI Installer, en cliquant sur "Go to the Download Page"

4 - Choisissez la version ayant le plus de téléchargement.

5 - Activer l'exécutable et choisir la version "Developer Default".

6- Cliquer sur "Execute".

7 - Continuer, jusqu'à ce qu'il sera demander de créer un mot de passe pour le "root", faites-le.

8 - Continuer, jusqu'à ce qu'il vous demande de valider votre mot de passe avec "Check", puis continuer.

9 - Continuer, jusqu'à ce que l'installation soit compléter.

L'installation de MySQL est enfin terminée.

-----------------------------------------------------------------------------------------------------------------

INSTALLATION DE GIT

1 - Rechercher git et aller sur le site de celui-ci.

2- Cliquer sur le bouton "Download".

3- Activer l'exécutable, et peser sur "Next" jusqu'à "Install" apparaît, puis terminer l'installation.

---------------------------------------------------------------------------------------------------------------

INSTALLER PYTHON

1 - Rechercher python, pour se rendre au site.

2 - Aller télécharger la version de Python la plus récente.

3 - Activer l'exécutable, et continuer les étapes.

4 - Lorsque le tout est terminé, fermer-le.

---------------------------------------------------------------------------------------------------------------------


ALLER CHERCHER LE PROJET GIT

2 -  Rentrer ce lien dans votre navigateur, pour accéder au GIT : "https://FrancoeurS@bitbucket.org/FrancoeurS/b65_sacha_francoeur.git"

3 - Cliquer sur le bouton "Clone" en haut à droite", puis copier l'info présente.

4 - Ouvrer Git Bash et coller le contenu de l'info présente, pis "Enter".

Le contenu du git devrait être alors sur votre ordinateur.

---------------------------------------------------------------------------------------------------------------------

POUR SE CONNECTER À MYSQL ET CRÉÉ LES TABLES

1 - Ouvrez MySQL WorkBench.

2 - Cliquer sur le "Local Instance", puis entrer votre mot de passe.

3 - Aller dans la Query1 et puis rentrer ses trois lignes de commande :

	CREATE USER 'conju'@'localhost' IDENTIFIED BY 'conjuGate';
	GRANT ALL privileges ON CONJU.* to 'conju'@'localhost';
	ALTER USER 'conju'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';

4 - Aller dans File et cliquer sur "Open SQL Script"

5 - Choississez le fichier SQL, table.sql et faire ouvrir.

6 - Cliquer sur l'icône avec l'éclair sans curseur pour faire rouler le script de toute la page.

7 - Répétez l'étape 5, mais cette fois-ci avec verbe.sql.

8 - Prener les deux premières et les exécuter avec l'icône de l'éclair avec un curseur, une à la fois.

9 - Répétez les deux dernières étapes, mais avec les 4 autres fichiers (present, imparfait, phrase_present_facile, phrase_imparfait_facile)

L'insertion des données et la création de tables MySQL fonctionne, maintenant.

------------------------------------------------------------------------------------------------------------------

POUR FAIRE FONCTIONNER LE PROJET

1 - Créer vous un dossier dans l'endroit que vous souhaitez.

2 - La fenêtre PyCharm s'affiche, cliquez sur "Create New Project", puis trouver le dossier correspondant à celui que vous vener de crée.

4 - Cliquer sur la flèche contenant "Projet Interpreter: New Virtualenv environment"

5 - Dans Location, choississez le chemin que vous avez créer pour le dossier

6 - Assurer que l'interpréteur Python est le bon.

7 - Cliquer sur "Create".

8 - Copier les fichiers Python provenant du dossier GIT et copier les dans le dossier créée.

9 - Aller sur le fichier main.py, en voyant celui-ci vous devriez voir PyQt5 en rouge.

10 - Vous allez voir "Install package PyQt5", cliquer dessus.
	Attendez la fin de l'installation.

11 - Aller en haut à gauche dans "Files", puis aller dans "Settings".

12 - Cliquer sur "Projet: Nom de votre Projet", puis "Project Interpreter".

13 - Cliquer ensuite sur l'icône avec un +.

14 - Inscriver mysql-connector dans la barre de recherche et faite "Install Package" sur celui-ci".

15 - Faite un clic droit dans le main, pis faite Run "main". (Ou Ctrl+Maj+F10)

MAINTENANT, le tout devrait fonctionné.