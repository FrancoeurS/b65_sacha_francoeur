#	1. ÊTRE 
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(1,'fus','fus','fut','fûmes','fûtes','furent');
#	2. 	AVOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(2,'eus','eus','eut','eûmes','eûtes','eurent');
#	3. 	FAIRE
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(3,'fis','fis','fit','fîmes','fîtes','firent');
#   4. 	DIRE
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(4,'dis','dis','dit','dîmes','dîtes','dirent');
#	5. 	POUVOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(5,'pus','pus','put','pûmes','pûtes','purent');
#	6. 	ALLER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(6,'allai','allas','alla','allâmes','allâtes','allèrent');
#	7. 	VOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(7,'vis','vis','vit','vîmes','vîtes','virent');
#	8. 	SAVOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(8,'sus','sus','sut','sûmes','sûtes','surent');
#	9. 	VOULOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(9,'voulus','voulus','voulut','voulûmes','voulûtes','voulurent');
#	10. VENIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(10,'vins','vins','vint','vînmes','vîntes','vinrent');
#	11. FALLOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(11,null,null,'fallut',null,null,null);
#	12.	DEVOIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(12,'dus','dus','dut','dûmes','dûtes','durent');
#	13.	CROIRE
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(13,'crus','crus','crut','crûmes','crûtes','crurent');
#	14.	TROUVER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(14,'trouvai','trouvas','trouva','trouvâmes','trouvâtes','trouvèrent');
#	15.	DONNER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(15,'donnai','donnas','donna','donnâmes','donnâtes','donnèrent');
#	16. PRENDRE
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(16,'pris','pris','prit','prîmes','prîtes','prirent');
#	17.	PARLER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(17,'parlai','parlas','parla','parlâmes','parlâtes','parlèrent');
#	18. AIMER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(18,'aimai','aimas','aima','aimâmes','aimâtes','aimèrent');
#	19.	PASSER
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(19,'passai','passas','passa','passâmes','passâtes','passèrent');
#	20.	METTRE
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(20,'mis','mis','mit','mîmes','mîtes','mirent');
#	21.	FINIR
INSERT INTO passe_simple(id_verbe, passe_sp_1ps, passe_sp_2ps,
passe_sp_3ps, passe_sp_1pp, passe_sp_2pp, passe_sp_3pp) VALUES
(21,'finis','finis','finit','finîmes','finîtes','finirent');