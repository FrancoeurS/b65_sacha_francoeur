#	1. ÊTRE 
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(1,'sois','sois','soit','soyons','soyez','soient');
#	2. 	AVOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(2,'aie','aies','ait','ayons','ayez','aient');
#	3. 	FAIRE
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(3,'fasse','fasses','fasse','fassions','fassiez','fassent');
#   4. 	DIRE
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(4,'dise','dises','dise','disions','disiez','disent');
#	5. 	POUVOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(5,'puisse','puisses','puisse','puissions','puissiez','puissent');
#	6. 	ALLER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(6,'aille','ailles','aille','allions','alliez','aillent');
#	7. 	VOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(7,'voie','voies','voie','voyions','voyiez','voient');
#	8. 	SAVOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(8,'sache','saches','sache','sachions','sachiez','sachent');
#	9. 	VOULOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(9,'veuille','veuilles','veuille','voulions','vouliez','veuillent');
#	10. VENIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(10,'vienne','viennes','vienne','venions','veniez','viennent');
#	11. FALLOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(11,null,null,faille,null,null,null);
#	12.	DEVOIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(12,'doive','doives','doive','devions','deviez','doivent');
#	13.	CROIRE
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(13,'croie','croies','croie','croyions','croyiez','croient');
#	14.	TROUVER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(14,'trouve','trouves','trouve','trouvions','trouviez','trouvent');
#	15.	DONNER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(15,'donne','donnes','donne','donnions','donniez','donnent');
#	16. PRENDRE
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(16,'prenne','prennes','prenne','prenions','preniez','prennent');
#	17.	PARLER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(17,'parle','parles','parle','parlions','parliez','parlent');
#	18. AIMER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(18,'aime','aimes','aime','aimions','aimiez','aiment');
#	19.	PASSER
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(19,'passe','passes','passe','passions','passiez','passent');
#	20.	METTRE
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(20,'mette','mettes','mette','mettions','mettiez','mettent');
#	21.	FINIR
INSERT INTO subjonctif_present(id_verbe, subj_pres_1ps, subj_pres_2ps,
subj_pres_3ps, subj_pres_1pp, subj_pres_2pp, subj_pres_3pp) VALUES
(21,'finisse','finisses','finisse','finissions','finissiez','finissent');