#	1. ÊTRE 
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(1,'serai','seras','sera','serons','serez','seront');
#	2. 	AVOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(2,'aurai','auras','aura','aurons','aurez','auront');
#	3. 	FAIRE
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(3,'ferai','feras','fera','ferons','ferez','feront');
#   4. 	DIRE
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(4,'dirai','diras','dira','dirons','direz','diront');
#	5. 	POUVOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(5,'pourrai','pourras','pourra','pourrons','pourrez','pourront');
#	6. 	ALLER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(6,'irai','iras','ira','irons','irez','iront');
#	7. 	VOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(7,'verrai','verras','verra','verrons','verrez','verront');
#	8. 	SAVOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(8,'saurai','sauras','saura','saurons','saurez','sauront');
#	9. 	VOULOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(9,'voudrai','voudras','voudra','voudrons','voudrez','voudront');
#	10. VENIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(10,'viendrai','viendras','viendra','viendrons','viendrez','viendront');
#	11. FALLOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(11,null,null,'faudra',null,null,null);
#	12.	DEVOIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(12,'devrai','devras','devra','devrons','devrez','devront');
#	13.	CROIRE
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(13,'croirai','croiras','croira','croirons','croirez','croiront');
#	14.	TROUVER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(14,'trouverai','trouveras','trouvera','trouverons','trouverez','trouveront');
#	15.	DONNER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(15,'donnerai','donneras','donnera','donnerons','donnerez','donneront');
#	16. PRENDRE
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(16,'prendrai','prendras','prendra','prendrons','prendrez','prendront');
#	17.	PARLER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(17,'parlerai','parleras','parlera','parlerons','parlerez','parleront');
#	18. AIMER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(18,'aimerai','aimeras','aimera','aimerons','aimerez','aimeront');
#	19.	PASSER
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(19,'passerai','passeras','passera','passerons','passerez','passeront');
#	20.	METTRE
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(20,'mettrai','mettras','mettra','mettrons','mettrez','mettront');
#	21. FINIR
INSERT INTO futur_simple(id_verbe, futur_sp_1ps, futur_sp_2ps,
futur_sp_3ps, futur_sp_1pp, futur_sp_2pp, futur_sp_3pp) VALUES
(21,'finirai','finiras','finira','finirons','finirez','finiront');