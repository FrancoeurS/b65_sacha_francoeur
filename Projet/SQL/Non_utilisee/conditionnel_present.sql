#	1. ÊTRE 
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(1,'serais','serais','serait','serions','seriez','seraient');
#	2. 	AVOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(2,'aurais','aurais','aurait','aurions','auriez','auraient');
#	3. 	FAIRE
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(3,'ferais','ferais','ferait','ferions','feriez','feraient');
#   4. 	DIRE
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(4,'dirais','dirais','dirait','dirions','diriez','diraient');
#	5. 	POUVOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(5,'pourrais','pourrais','pourrait','pourrions','pourriez','pourraient');
#	6. 	ALLER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(6,'irais','irais','irait','irions','iriez','iraient');
#	7. 	VOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(7,'verrais','verrais','verrait','verrions','verriez','verraient');
#	8. 	SAVOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(8,'saurais','saurais','saurait','saurions','sauriez','sauraient');
#	9. 	VOULOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(9,'voudrais','voudrais','voudrait','voudrions','voudriez','voudraient');
#	10. VENIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(10,'viendrais','viendrais','viendrait','viendrions','viendriez','viendraient');
#	11. FALLOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(11,null,null,faudrait,null,null,null);
#	12.	DEVOIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(12,'devrais','devrais','devrait','devrions','devriez','devraient');
#	13.	CROIRE
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(13,'croirais','croirais','croirait','croirions','croiriez','croiraient');
#	14.	TROUVER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(14,'trouverais','trouverais','trouverait','trouverions','trouveriez','trouveraient');
#	15.	DONNER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(15,'donnerais','donnerais','donnerait','donnerions','donneriez','donneraient');
#	16. PRENDRE
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(16,'prendrais','prendrais','prendrait','prendrions','prendriez','prendraient');
#	17.	PARLER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(17,'parlerais','parlerais','parlerait','parlerions','parleriez','parleraient');
#	18. AIMER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(18,'aimerais','aimerais','aimerait','aimerions','aimeriez','aimeraient');
#	19.	PASSER
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(19,'passerais','passerais','passerait','passerions','passeriez','passeraient');
#	20.	METTRE
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(20,'mettrais','mettrais','mettrait','mettrions','mettriez','mettraient');
#	21.	FINIR
INSERT INTO conditionnel_present(id_verbe, cond_pres_1ps, cond_pres_2ps,
cond_pres_3ps, cond_pres_1pp, cond_pres_2pp, cond_pres_3pp) VALUES
(21,'finirais','finirais','finirait','finirions','finirions','finiraient');