#	1. ÊTRE 
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(1,'sois','soyons','soyez');
#	2. 	AVOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(2,'aie','ayons','ayez');
#	3. 	FAIRE
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(3,'fais','faisons','faites');
#   4. 	DIRE
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(4,'dis','disons','dites');
#	5. 	POUVOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(5,null,null,null);
#	6. 	ALLER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(6,'va','allons','allez');
#	7. 	VOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(7,'vois','voyons','voyez');
#	8. 	SAVOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(8,'sache','sachons','sachez');
#	9. 	VOULOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(9,'veux','voulons','voulez');
#	10. VENIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(10,'viens','venons','venez');
#	11. FALLOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(11,null,null,null);
#	12.	DEVOIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(12,'dois','devons','devez');
#	13.	CROIRE
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(13,'crois','croyons','croyez');
#	14.	TROUVER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(14,'trouve','trouvons','trouvez');
#	15.	DONNER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(15,'donne','donnons','donnez');
#	16. PRENDRE
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(16,'prends','prenons','prenez');
#	17.	PARLER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(17,'parle','parlons','parlez');
#	18. AIMER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(18,'aime','aimons','aimez');
#	19.	PASSER
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(19,'passe','passons','passez');
#	20.	METTRE
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(20,'mets','mettons','mettez');
#	21.	FINIR
INSERT INTO imperatif_present(id_verbe, imper_pres_1ps, imper_pres_2ps,
imper_pres_3ps) VALUES
(21,'finis','finissons','finissez');
