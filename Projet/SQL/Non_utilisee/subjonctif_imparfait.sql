#	1. ÊTRE 
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(1,'fusse','fusses','fût','fussions','fussiez','fussent');
#	2. 	AVOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(2,'eusse','eusses','eût','eussions','eussiez','eussent');
#	3. 	FAIRE
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(3,'fisse','fisses','fît','fissions','fissiez','fissent');
#   4. 	DIRE
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(4,'disse','disses','dît','dissions','dissiez','dissent');
#	5. 	POUVOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(5,'pusse','pusses','pût','pussions','pussiez','pussent');
#	6. 	ALLER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(6,'allasse','allasses','allât','allassions','allassiez','allassent');
#	7. 	VOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(7,'visse','visses','vît','vissions','vissiez','vissent');
#	8. 	SAVOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(8,'susse','susses','sût','sussions','sussiez','sussent');
#	9. 	VOULOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(9,'voulusse','voulusses','voulût','voulussions','voulussiez','voulussent');
#	10. VENIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(10,'vinsse','vinsses','vînt','vinssions','vinssiez','vinssent');
#	11. FALLOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(11,null,null,'fallût',null,null,null);
#	12.	DEVOIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(12,'dusse','dusses','dût','dussions','dussiez','dussent');
#	13.	CROIRE
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(13,'crusse','crusses','crût','crussions','crussiez','crussent');
#	14.	TROUVER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(14,'trouvasse','trouvasses','trouvât','trouvassions','trouvassiez','trouvassent');
#	15.	DONNER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(15,'donnasse','donnasses','donnât','donnassions','donnassiez','donnassent');
#	16. PRENDRE
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(16,'prisse','prisses','prît','prissions','prissiez','prissent');
#	17.	PARLER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(17,'parlasse','parlasses','parlât','parlassions','parlassiez','parlassent');
#	18. AIMER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(18,'aimasse','aimasses','aimât','aimassions','aimassiez','aimassent');
#	19.	PASSER
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(19,'passasse','passasses','passât','passassions','passassiez','passassent');
#	20.	METTRE
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(20,'misse','misses','mît','missions','missiez','missent');
#	21.	FINIR
INSERT INTO subjonctif_imparfait(id_verbe, subj_impf_1ps, subj_impf_2ps,
subj_impf_3ps, subj_impf_1pp, subj_impf_2pp, subj_impf_3pp) VALUES
(21,'finisse','finisses','finît','finissions','finissiez','finissent');