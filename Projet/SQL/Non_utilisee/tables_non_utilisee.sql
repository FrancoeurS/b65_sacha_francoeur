CREATE TABLE progression (
	id INT NOT NULL auto_increment,
    id_utilisateur INT(6) NOT NULL,
    niveau INT(2),
    date_connexion DATE,
    experience INT(6),
    exp_prochain_niveau INT(6),
    PRIMARY KEY(ID),
    CONSTRAINT fk_prog_id_utilisateur FOREIGN KEY (id_utilisateur)
	REFERENCES utilisateurs(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


CREATE TABLE passe_simple (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	passe_simple_1ps VARCHAR(26),
	passe_simple_2ps VARCHAR(26),
	passe_simple_3ps VARCHAR(26),
	passe_simple_1pp VARCHAR(26),
	passe_simple_2pp VARCHAR(26),
	passe_simple_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_passe_sp_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE futur_simple (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	futur_simple_1ps VARCHAR(26),
	futur_simple_2ps VARCHAR(26),
	futur_simple_3ps VARCHAR(26),
	futur_simple_1pp VARCHAR(26),
	futur_simple_2pp VARCHAR(26),
	futur_simple_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_futur_sp_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE conditionnel_present (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	conditionnel_present_1ps VARCHAR(26),
	conditionnel_present_2ps VARCHAR(26),
	conditionnel_present_3ps VARCHAR(26),
	conditionnel_present_1pp VARCHAR(26),
	conditionnel_present_2pp VARCHAR(26),
	conditionnel_present_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_cond_pres_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE subjonctif_present (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	subjonctif_present_1ps VARCHAR(26),
	subjonctif_present_2ps VARCHAR(26),
	subjonctif_present_3ps VARCHAR(26),
	subjonctif_present_1pp VARCHAR(26),
	subjonctif_present_2pp VARCHAR(26),
	subjonctif_present_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_subj_pres_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE subjonctif_imparfait (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	subjonctif_imparfait_1ps VARCHAR(26),
	subjonctif_imparfait_2ps VARCHAR(26),
	subjonctif_imparfait_3ps VARCHAR(26),
	subjonctif_imparfait_1pp VARCHAR(26),
	subjonctif_imparfait_2pp VARCHAR(26),
	subjonctif_imparfait_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_subj_impf_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE imperatif_present (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	imperatif_present_2ps VARCHAR(26),
	imperatif_present_1pp VARCHAR(26),
	imperatif_present_2pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_subj_impf_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;