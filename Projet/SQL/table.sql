CREATE DATABASE conju;
USE conju;


CREATE TABLE utilisateurs (
	id INT NOT NULL auto_increment,
    nom_utilisateur VARCHAR(32),
    prenom VARCHAR(32),
    nom VARCHAR(32),
    email VARCHAR(32),
	mot_passe VARCHAR(32),
    primary key(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE verbe (
	id INT NOT NULL AUTO_INCREMENT,
    infinitif VARCHAR(26),
	auxiliaire VARCHAR(26),
	groupe INT(1),
    participe_present VARCHAR(26),
    PRIMARY KEY (id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE present (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	present_1ps VARCHAR(26),
	present_2ps VARCHAR(26),
	present_3ps VARCHAR(26),
	present_1pp VARCHAR(26),
	present_2pp VARCHAR(26),
	present_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_present_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE imparfait (
	id INT NOT NULL AUTO_INCREMENT,
    id_verbe INT(6),
	imparfait_1ps VARCHAR(26),
	imparfait_2ps VARCHAR(26),
	imparfait_3ps VARCHAR(26),
	imparfait_1pp VARCHAR(26),
	imparfait_2pp VARCHAR(26),
	imparfait_3pp VARCHAR(26),
    PRIMARY KEY (id),
	CONSTRAINT fk_impf_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;

CREATE TABLE phrase_present_facile (
	id INT NOT NULL AUTO_INCREMENT,
	id_utilisateur INT(6),
    id_verbe INT(6),
	phrase1 VARCHAR(255),
	phrase2 VARCHAR(255),
	phrase3 VARCHAR(255),
	phrase4 VARCHAR(255),
	phrase5 VARCHAR(255),
    phrase6 VARCHAR(255),
	PRIMARY KEY (id),
	CONSTRAINT fk_ph_fac_present_id_utilisateur FOREIGN KEY (id_utilisateur)
	REFERENCES utilisateurs(id),
    CONSTRAINT fk_ph_fac_present_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8; 

CREATE TABLE phrase_imparfait_facile (
	id INT NOT NULL AUTO_INCREMENT,
	id_utilisateur INT(6),
    id_verbe INT(6),
	phrase1 VARCHAR(255),
	phrase2 VARCHAR(255),
	phrase3 VARCHAR(255),
	phrase4 VARCHAR(255),
	phrase5 VARCHAR(255),
    phrase6 VARCHAR(255),
	PRIMARY KEY (id),
	CONSTRAINT fk_ph_fac_imparfait_id_utilisateur FOREIGN KEY (id_utilisateur)
	REFERENCES utilisateurs(id),
    CONSTRAINT fk_ph_fac_imparfait_id_verbe FOREIGN KEY (id_verbe)
	REFERENCES verbe(id)
) ENGINE = innoDB DEFAULT CHARSET=utf8;
