INSERT INTO phrase_present_facile(id_utilisateur, id_verbe, phrase1, phrase2, phrase3, phrase4, phrase5, phrase6)
VALUES (1,1,"Je suis une personne disponible pour vous aider.","Tu es une personne très méchante.",
"Il est présentement trois heures et un quart.","Nous sommes une équipe qui savent former des travailleurs uniques.",
"Vous êtes une belle famille.","Ils sont tous des étudiants de la technique d'informatique du Cégep du Vieux-Montréal.");

INSERT INTO phrase_present_facile(id_utilisateur, id_verbe, phrase1, phrase2, phrase3, phrase4, phrase5, phrase6)
VALUES (1,2,"J' ai mon ordinateur avec moi, présentement.","Tu as le livre de mes parents.",
"Christine a un emploi d'informaticienne chez Ubisoft.","Nous avons une compagnie qui priorise le travail d'équipe.",
"Vous avez des scripts disponibles sur Léa.","À la BANQ, ils ont des livres d'une valeur inestimable.");
