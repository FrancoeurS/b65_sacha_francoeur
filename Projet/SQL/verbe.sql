#	1. 	ÊTRE 
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('être','avoir',3,'étant');
#	2. 	AVOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('avoir','avoir',3,'ayant');
#	3. 	FAIRE
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('faire','avoir',3,'faisant');
#   4. 	DIRE
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('dire','avoir',3,'disant');
#	5. 	POUVOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('pouvoir','avoir','3','pouvant');
#	6. 	ALLER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('aller','être','3','allant');
#	7. 	VOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('voir','avoir',3,'voyant');
#	8. 	SAVOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('savoir','avoir',3,'sachant');
#	9. 	VOULOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('vouloir','avoir',3,'voulant');
#	10. VENIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('venir','être',3,'venant');
#	11. FALLOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('falloir','avoir',4,null);
#	12.	DEVOIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('devoir','avoir',3,'devant');
#	13.	CROIRE
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('croire','avoir',3,'croyant');
#	14.	TROUVER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('trouver','avoir',1,'trouvant');
#	15.	DONNER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('donner','avoir',1,'donnant');
#	16. PRENDRE
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('prendre','avoir',3,'prenant');
#	17.	PARLER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('parler','avoir',1,'parlant');
#	18. AIMER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('aimer','avoir',1,'aimant');
#	19.	PASSER
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('passer','avoir',1,'passant');
#	20.	METTRE
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('mettre','avoir',3,'mettant');
#	21. FINIR
INSERT INTO verbe(infinitif, auxiliaire , groupe, participe_present) VALUES
('finir','avoir'2,'finissant');