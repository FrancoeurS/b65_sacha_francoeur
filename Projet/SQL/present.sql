# SEULEMENT ÊTRE ET AVOIR ONT ÉTÉ UTILISÉE POUR FAIRE LES TEST DURANT LE PROJET
#	1. ÊTRE 
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(1,'suis','es','est','sommes','êtes','sont');
#	2. 	AVOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(2,'ai','as','a','avons','avez','ont');
#	3. 	FAIRE
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(3,'fais','fais','fait','faisons','faites','font');
#   4. 	DIRE
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(4,'dis','dis','dit','disons','dites','disent');
#	5. 	POUVOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(5,'peux','peux','peut','pouvons','pouvez','peuvent');
#	6. 	ALLER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(6,'vais','vas','va','allons','allez','vont');
#	7. 	VOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(7,'vois','vois','voit','voyons','voyez','voient');
#	8. 	SAVOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(8,'sais','sais','sait','savons','savez','savent');
#	9. 	VOULOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(9,'veux','veux','veut','voulons','voulez','veulent');
#	10. VENIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(10,'viens','viens','vient','venons','venez','viennent');
#	11. FALLOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(11,null,null,'faut',null,null,null);
#	12.	DEVOIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(12,'dois','dois','doit','devons','devez','doivent');
#	13.	CROIRE
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(13,'crois','crois','croit','croyons','croyez','croient');
#	14.	TROUVER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(14,'trouve','trouves','trouve','trouvons','trouvez','trouvent');
#	15.	DONNER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(15,'donne','donnes','donne','donnons','donnez','donnent');
#	16. PRENDRE
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(16,'prends','prends','prend','prenons','prenez','prennent');
#	17.	PARLER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(17,'parle','parles','parle','parlons','parlez','parlent');
#	18. AIMER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(18,'aime','aimes','aime','aimons','aimez','aiment');
#	19.	PASSER
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(19,'passe','passes','passe','passons','passez','passent');
#	20.	METTRE
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(20,'mets','mets','met','mettons','mettez','mettent');
#	21. FINIR
INSERT INTO present(id_verbe, present_1ps, present_2ps,
present_3ps, present_1pp, present_2pp, present_3pp) VALUES
(21,'finis','finis','finit','finissons','finissez','finissent');