# SEULEMENT ÊTRE ET AVOIR ONT ÉTÉ UTILISÉE POUR FAIRE LES TEST DURANT LE PROJET
#	1. ÊTRE 
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(1,'étais','étais','était','étions','étiez','étaient');
#	2. 	AVOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(2,'avais','avais','avait','avions','aviez','avaient');
#	3. 	FAIRE
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(3,'faisais','faisais','faisait','faisions','faisiez','faisaient');
#   4. 	DIRE
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(4,'disais','disais','disait','disions','disiez','disaient');
#	5. 	POUVOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(5,'pouvais','pouvais','pouvait','pouvions','pouviez','pouvaient');
#	6. 	ALLER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(6,'allais','allais','allait','allions','alliez','allaient');
#	7. 	VOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(7,'voyais','voyais','voyait','voyions','voyiez','voyaient');
#	8. 	SAVOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(8,'savais','savais','savait','savions','saviez','savaient');
#	9. 	VOULOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(9,'voulais','voulais','voulait','voulions','vouliez','voulaient');
#	10. VENIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(10,'venais','venais','venait','venions','veniez','venaient');
#	11. FALLOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(11,null,null,'fallait',null,null,null);
#	12.	DEVOIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(12,'devais','devais','devait','devions','deviez','devaient');
#	13.	CROIRE
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(13,'croyais','croyais','croyait','croyions','croyiez','croyaient');
#	14.	TROUVER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(14,'trouvais','trouvais','trouvait','trouvions','trouviez','trouvaient');
#	15.	DONNER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(15,'donnais','donnais','donnait','donnions','donniez','donnaient');
#	16. PRENDRE
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(16,'prenais','prenais','prenait','prenions','preniez','prenaient');
#	17.	PARLER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(17,'parlais','parlais','parlait','parlions','parliez','parlaient');
#	18. AIMER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(18,'aimais','aimais','aimait','aimions','aimiez','aimaient');
#	19.	PASSER
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(19,'passais','passais','passait','passions','passiez','passaient');
#	20.	METTRE
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(20,'mettais','mettais','mettait','mettions','mettiez','mettaient');
#	21.	FINIR
INSERT INTO imparfait(id_verbe, imparfait_1ps, imparfait_2ps,
imparfait_3ps, imparfait_1pp, imparfait_2pp, imparfait_3pp) VALUES
(21,'finissais','finissais','finissait','finissions','finissiez','finissaient');