INSERT INTO phrase_imparfait_facile(id_utilisateur, id_verbe, phrase1, phrase2, phrase3, phrase4, phrase5, phrase6)
VALUES (1,1,"J' étais à l'école samedi dernier, pour finir mon Projet Synthèse.","Tu étais parmis les représentatifs du DEC intensif.",
"Jean-Christophe était disponible pour répondre aux questions des étudiants par MIO.","Nous étions tous présents lors de la présentation des projets synthèse.",
"Vous êtiez à l'école pour finir les derniers projets à remettre.","Ils étaient les étudiants de la session de la cohorte d'Automne 2018.");

INSERT INTO phrase_imparfait_facile(id_utilisateur, id_verbe, phrase1, phrase2, phrase3, phrase4, phrase5, phrase6)
VALUES (1,2,"J' avais une ordinateur de la marque ASUS, quelques années plus tôt.","Tu avais un sac à dos bleu, dernièrement.",
"Christine avait des choses à te dire.","Nous avions des problèmes d'indentations.",
"Vous aviez des actions dans la compagnie ANONYME.","Ils avaient des logiciels de bonne qualité au Cégep.");