from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QSizePolicy, QPushButton

import home


class Stats(QWidget):
    def __init__(self, data):
        super().__init__()
        self.title = "Statistiques"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.margin = 100

        self.msgVerbMU = "Verbe le plus pratiqué"
        self.msgTimeMU = "Temps le plus pratiqué"
        self.msgVerbMS = "Verbe le plus recherché"
        self.msgVerbLU = "Dernier verbe pratiqué"
        self.msgTimeLU = "Dernier temps pratiqué"
        self.msgVerbLS = "Dernier verbe recherché"
        self.msgMethPref = "Méthode de saisie préféré"
        self.msgHome = "Accueil"
        self.writingFont = "Helvetica"
        self.fontSize = 18
        self.colorOrange = "color:orange"

        self.mainLayout = QGridLayout()
        self.lblVerbMU = QLabel(self.msgVerbMU, self)
        self.lblInfoVerbMU = QLabel("", self)
        self.lblTimeMU = QLabel(self.msgTimeMU, self)
        self.lblInfoTimeMU = QLabel("", self)
        self.lblVerbMS = QLabel(self.msgVerbMS, self)
        self.lblInfoVerbMS = QLabel("", self)
        self.lblVerbLU = QLabel(self.msgVerbLU, self)
        self.lblInfoVerbLU = QLabel("", self)
        self.lblTimeLU = QLabel(self.msgTimeLU, self)
        self.lblInfoTimeLU = QLabel("", self)
        self.lblVerbLS = QLabel(self.msgVerbLS, self)
        self.lblInfoVerbLS = QLabel("", self)
        self.lblMethPref = QLabel(self.msgMethPref, self)
        self.lblInfoMethPref = QLabel("", self)
        self.btnHome = QPushButton(self.msgHome, self)
        self.font = QFont(self.writingFont, self.fontSize)

        self.data = data

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()

        self.btnHome.clicked.connect(self.home)

    def home(self):
        self.w = home.Home(self.data)
        self.w.info()
        self.w.show()
        self.hide()

    def fontOnEverything(self):
        self.lblVerbMU.setFont(self.font)
        self.lblInfoVerbMU.setFont(self.font)
        self.lblInfoVerbMU.setStyleSheet(self.colorOrange)
        self.lblTimeMU.setFont(self.font)
        self.lblInfoTimeMU.setFont(self.font)
        self.lblInfoTimeMU.setStyleSheet(self.colorOrange)
        self.lblVerbMS.setFont(self.font)
        self.lblInfoVerbMS.setFont(self.font)
        self.lblInfoVerbMS.setStyleSheet(self.colorOrange)
        self.lblVerbLU.setFont(self.font)
        self.lblInfoVerbLU.setFont(self.font)
        self.lblInfoVerbLU.setStyleSheet(self.colorOrange)
        self.lblTimeLU.setFont(self.font)
        self.lblInfoTimeLU.setFont(self.font)
        self.lblInfoTimeLU.setStyleSheet(self.colorOrange)
        self.lblVerbLS.setFont(self.font)
        self.lblInfoVerbLS.setFont(self.font)
        self.lblInfoVerbLS.setStyleSheet(self.colorOrange)
        self.lblMethPref.setFont(self.font)
        self.lblInfoMethPref.setFont(self.font)
        self.lblInfoMethPref.setStyleSheet(self.colorOrange)
        self.btnHome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblVerbMU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoVerbMU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblTimeMU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoTimeMU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbMS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoVerbMS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbLU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoVerbLU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblTimeLU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoTimeLU.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbLS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoVerbLS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblMethPref.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoMethPref.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblVerbMU, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoVerbMU, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lblVerbLU, 1, 2, 1, 1)
        self.mainLayout.addWidget(self.lblInfoVerbLU, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.lblTimeMU, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoTimeMU, 4, 0, 1, 1)
        self.mainLayout.addWidget(self.lblTimeLU, 3, 2, 1, 1)
        self.mainLayout.addWidget(self.lblInfoTimeLU, 4, 2, 1, 1)
        self.mainLayout.addWidget(self.lblVerbMS, 5, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoVerbMS, 6, 0, 1, 1)
        self.mainLayout.addWidget(self.lblVerbLS, 5, 2, 1, 1)
        self.mainLayout.addWidget(self.lblInfoVerbLS, 6, 2, 1, 1)
        self.mainLayout.addWidget(self.lblMethPref, 7, 1, 1, 1)
        self.mainLayout.addWidget(self.lblInfoMethPref, 8, 1, 1, 1)
