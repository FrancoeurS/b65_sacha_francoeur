import sys
#sys.path.append("C:/Users/Sacha/Desktop/git/b65_sacha_francoeur/Projet/Conju/venv_home/Lib/site-packages")
sys.path.append("../Conju/venv_home/Lib/site-packages")
#sys.path.append("venv_home/Lib/site-packages")
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import (QPushButton,QLabel, QSizePolicy, QGridLayout, QWidget)
import connectUser
import createUser


class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.title = "Conju"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.margin = 100
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgCreate = "Création d'un utilisateur"
        self.msgConnect = "Connexion"
        self.msgConju = "Conju"

        self.font = QFont()
        self.mainLayout = QGridLayout()
        self.label = QLabel(self.msgConju, self)
        self.btnCreate = QPushButton(self.msgCreate, self)
        self.btnConnect = QPushButton(self.msgConnect, self)
        self.font = QFont("Helvetica", 18)
        self.titleFont = QFont("Arial", 32)
        self.colorOrange = "color:orange"

        self.info()

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.getConnection()
        self.show()

    def windowCreate(self):
        self.w = createUser.WindowCreate()
        self.w.info()
        self.w.show()
        self.hide()

    def windowConnect(self):
        self.w = connectUser.WindowConnect()
        self.w.info()
        self.w.show()
        self.hide()

    def getConnection(self):
        self.btnCreate.clicked.connect(self.windowCreate)
        self.btnConnect.clicked.connect(self.windowConnect)

    def fontOnEverything(self):
        self.label.setStyleSheet(self.colorOrange)
        self.label.setFont(self.titleFont)
        self.btnConnect.setFont(self.font)
        self.btnCreate.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.btnCreate.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnConnect.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.label)
        self.mainLayout.addWidget(self.btnCreate)
        self.mainLayout.addWidget(self.btnConnect)



