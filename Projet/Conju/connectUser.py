from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QLabel, QPushButton, QLineEdit, QSizePolicy, QGridLayout, QWidget, QMessageBox
import connection_pooling
import home
import welcome


class WindowConnect(QWidget):
    def __init__(self):
        super().__init__()
        self.title = "Connexion de l'utilisateur"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgUsername = "Nom de l'utilisateur"
        self.msgPassword = "Mot de passe"
        self.msgConnect = "Se connecter"
        self.msgError = "Erreur"
        self.msginvData = "Un des vos données est invalide"
        self.msgReturn = "Retour"

        self.btnConnect = QPushButton(self.msgConnect, self)
        self.btnWelcome = QPushButton(self.msgReturn, self)
        self.lePwd = QLineEdit(self)
        self.lblPwd = QLabel(self.msgPassword, self)
        self.leUsername = QLineEdit(self)
        self.lblUsername = QLabel(self.msgUsername, self)
        self.mainLayout = QGridLayout()
        self.font = QFont("Helvetica", 18)
        self.margin = 100

        self.connPooling = connection_pooling.Connection_pooling()
        self.connexion = self.connPooling.conn
        self.cursor = self.connPooling.cursor
        self.users = "SELECT * from utilisateurs where nom_utilisateur=%s and mot_passe=%s"
        self.userInput = None
        self.data = None

    def info(self):
        self.fontOnEverything()
        self.lePwd.setEchoMode(QLineEdit.Password)
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.btnConnect.clicked.connect(self.validateConnection)
        self.btnWelcome.clicked.connect(self.welcome)

    def validateConnection(self):
        self.userInput = (self.leUsername.text(), self.lePwd.text())
        self.cursor.execute(self.users, self.userInput)
        self.data = self.cursor.fetchone()
        if self.data is not None:
            self.btnConnect.clicked.connect(self.home)
        else:
            self.quitMessageBox()
            self.leUsername.clear()
            self.lePwd.clear()

    def home(self):
        self.w = home.Home(self.data)
        self.w.info()
        self.w.show()
        self.hide()

    def welcome(self):
        self.w = welcome.Window()
        self.w.info()
        self.w.show()
        self.hide()

    def quitMessageBox(self):
        self.msgQuit = QMessageBox(self)
        self.msgQuit.setIcon(QMessageBox.Information)
        self.msgQuit.setText(self.msginvData)
        self.msgQuit.setWindowTitle(self.msgError)
        self.msgQuit.setStandardButtons(QMessageBox.Ok)
        self.msgQuit.show()

    def fontOnEverything(self):
        self.lblUsername.setFont(self.font)
        self.lblPwd.setFont(self.font)
        self.leUsername.setFont(self.font)
        self.lePwd.setFont(self.font)
        self.btnConnect.setFont(self.font)
        self.btnWelcome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblPwd.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.leUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lePwd.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnConnect.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnWelcome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnWelcome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblUsername, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.leUsername, 1, 2, 1, 1)
        self.mainLayout.addWidget(self.lblPwd, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lePwd, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.btnConnect, 3, 0, 1, 4)
