import sys

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QPushButton, QWidget, QSizePolicy, QGridLayout, QMessageBox, QApplication
import choix
import conjugueur
import infouser
import progress
import stats


class Home(QWidget):
    def __init__(self,data):
        super().__init__()
        self.title = "Accueil"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgExercise = "Exercice"
        self.msgSearch = "Recherche"
        self.msgDisconnect = "Déconnexion"
        self.msgInfoUser = "Info utilisateur"
        self.msgStats = "Statistiques"
        self.msgProgress = "Progression"
        self.msgReminding = "Souhaitez-vous vraiment quitter, toutes les informations sauront perdus?"
        self.writingFont = "Helvetica"
        self.fontSize = 18

        self.mainLayout = QGridLayout()
        self.msgQuit = QMessageBox(self)
        self.btnExercise = QPushButton(self.msgExercise, self)
        self.btnSearch = QPushButton(self.msgSearch, self)
        self.btnInfoUser = QPushButton(self.msgInfoUser, self)
        self.btnStats = QPushButton(self.msgStats, self)
        self.btnProg = QPushButton(self.msgProgress, self)
        self.btnDisc = QPushButton(self.msgDisconnect, self)
        self.font = QFont(self.writingFont, self.fontSize)

        self.userData = data

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.getButtonConnection()

    def getButtonConnection(self):
        self.btnInfoUser.clicked.connect(self.infoUser)
        self.btnStats.clicked.connect(self.stats)
        self.btnProg.clicked.connect(self.progress)
        self.btnExercise.clicked.connect(self.choice)
        self.btnSearch.clicked.connect(self.conjugueur)
        self.btnDisc.clicked.connect(self.disconnect)

    def choice(self):
        self.w = choix.Choix(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def conjugueur(self):
        self.w = conjugueur.Conjugueur(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def progress(self):
        self.w = progress.Progress(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def infoUser(self):
        self.w = infouser.InfoUser(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def stats(self):
        self.w = stats.Stats(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def disconnect(self):
        self.quitMessageBox()
        self.returnAnswer = self.msgQuit.exec()
        if self.returnAnswer == QMessageBox.Yes:
            print("Au revoir")
            app = QApplication(sys.argv)
            sys.exit(app.exec_())

    def quitMessageBox(self):
        self.msgQuit.setIcon(QMessageBox.Information)
        self.msgQuit.setText(self.msgReminding)
        self.msgQuit.setWindowTitle(self.msgDisconnect)
        self.msgQuit.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        self.msgQuit.show()

    def fontOnEverything(self):
        self.btnExercise.setFont(self.font)
        self.btnSearch.setFont(self.font)
        self.btnProg.setFont(self.font)
        self.btnInfoUser.setFont(self.font)
        self.btnStats.setFont(self.font)
        self.btnDisc.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.btnExercise.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnSearch.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnInfoUser.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnStats.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnProg.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnDisc.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnInfoUser, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.btnProg, 0, 1, 1, 1)
        self.mainLayout.addWidget(self.btnStats, 0, 2, 1, 1)
        self.mainLayout.addWidget(self.btnExercise, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.btnSearch, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.btnDisc, 3, 1, 1, 1)