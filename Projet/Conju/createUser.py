from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QLineEdit, QLabel, QPushButton, QGridLayout, QWidget, QSizePolicy, QMessageBox
import connection_pooling
import welcome
import re


class WindowCreate(QWidget):  # <===
    def __init__(self):
        super().__init__()
        self.title = "Création de l'utilisateur"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.margin = 100
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgUsername = "Nom de l'utilisateur"
        self.msgFirstName = "Prénom"
        self.msgLastName = "Nom"
        self.msgEmail = "Email"
        self.msgPassword = "Mot de passe"
        self.msgMinPwd = "Min de 8 caractères"
        self.msgSubscribe = "S'inscrire"
        self.msgError = "Erreur"
        self.msgSucInfo = "Succès"
        self.msginvData = "Un des vos données est invalide"
        self.msgSuccessCreate = "La création de votre utilisateur a réussi"
        self.msgReturn = "Retour"

        self.mainLayout = QGridLayout()
        self.btnWelcome = QPushButton(self.msgReturn, self)
        self.lblUsername = QLabel(self.msgUsername, self)
        self.leUsername = QLineEdit(self)
        self.lblFirstName = QLabel(self.msgFirstName, self)
        self.leFirstName = QLineEdit(self)
        self.lblLastName = QLabel(self.msgLastName, self)
        self.leLastName = QLineEdit(self)
        self.lblEmail = QLabel(self.msgEmail, self)
        self.leEmail = QLineEdit(self)
        self.lblPwd = QLabel(self.msgPassword, self)
        self.lePwd = QLineEdit(self)
        self.btnSubscribe = QPushButton(self.msgSubscribe, self)
        self.font = QFont("Helvetica", 18)

        self.connPooling = connection_pooling.Connection_pooling()
        self.connexion = self.connPooling.conn
        self.cursor = self.connPooling.cursor
        self.CREATEUSER = "INSERT INTO utilisateurs (nom_utilisateur,prenom,nom,email,mot_passe) VALUES (%s, %s, %s, %s, %s)"
        self.pattern = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.btnWelcome.clicked.connect(self.welcome)
        self.btnSubscribe.clicked.connect(self.createUser)

    def createUser(self):
        self.values = (
            self.leUsername.text(), self.leFirstName.text(), self.leLastName.text(), self.leEmail.text(),
            self.lePwd.text())
        if self.leUsername.text().__len__() <= 4 or self.leFirstName.text().__len__() <= 4 \
                or self.leLastName.text().__len__() <= 4 or self.leEmail.text().__len__() <= 4 \
                or not self.validitingEmail(self.leEmail.text()) or self.lePwd.text().__len__() <= 5:
            self.quitMessageBox()
            return False
        else:
            print("Here")
            self.successMessageBox()
            self.cursor.execute(self.CREATEUSER, self.values)
            self.connexion.commit()
            self.cursor.close()
            self.welcome()
            return True

    def validitingEmail(self, email):
        if re.search(self.pattern, email):
            return True
        else:
            return False

    def welcome(self):
        self.w = welcome.Window()
        self.w.info()
        self.w.show()
        self.hide()

    def successMessageBox(self):
        self.msgSuccess = QMessageBox(self)
        self.msgSuccess.setIcon(QMessageBox.Information)
        self.msgSuccess.setText(self.msgSuccessCreate)
        self.msgSuccess.setWindowTitle(self.msgSucInfo)
        self.msgSuccess.setStandardButtons(QMessageBox.Ok)
        self.msgSuccess.show()

    def quitMessageBox(self):
        self.msgQuit = QMessageBox(self)
        self.msgQuit.setIcon(QMessageBox.Information)
        self.msgQuit.setText(self.msginvData)
        self.msgQuit.setWindowTitle(self.msgError)
        self.msgQuit.setStandardButtons(QMessageBox.Ok)
        self.msgQuit.show()

    def fontOnEverything(self):
        self.lblUsername.setFont(self.font)
        self.leUsername.setFont(self.font)
        self.lblFirstName.setFont(self.font)
        self.leFirstName.setFont(self.font)
        self.lblLastName.setFont(self.font)
        self.leLastName.setFont(self.font)
        self.lblEmail.setFont(self.font)
        self.leEmail.setFont(self.font)
        self.lblPwd.setFont(self.font)
        self.btnSubscribe.setFont(self.font)
        self.lePwd.setFont(self.font)
        self.lePwd.setEchoMode(QLineEdit.Password)
        self.btnWelcome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblFirstName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblLastName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblEmail.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblPwd.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.leUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.leFirstName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.leLastName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.leEmail.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lePwd.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnSubscribe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnWelcome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnWelcome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblUsername, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.leUsername, 1, 2, 1, 1)
        self.mainLayout.addWidget(self.lblFirstName, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.leFirstName, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.lblLastName, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.leLastName, 3, 2, 1, 1)
        self.mainLayout.addWidget(self.lblEmail, 4, 0, 1, 1)
        self.mainLayout.addWidget(self.leEmail, 4, 2, 1, 1)
        self.mainLayout.addWidget(self.lblPwd, 5, 0, 1, 1)
        self.mainLayout.addWidget(self.lePwd, 5, 2, 1, 1)
        self.mainLayout.addWidget(self.btnSubscribe, 6, 0, 1, 4)
