from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QSizePolicy, QPushButton

import home


class Progress(QWidget):
    def __init__(self, data):
        super().__init__()
        self.title = "Informations de l'utilisateur"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgLvl = "Niveau"
        self.msgInfoLvl = ""
        self.msgExp = "Expérience"
        self.msgInfoExp = ""
        self.msgNextLvl = "Prochain niveau"
        self.msgInfoNextLvl = ""
        self.msgLastCon = "Dernière connexion(JJ/MM/AAAA)"
        self.msgInfoLastCon = ""
        self.msgHome = "Accueil"
        self.margin = 100
        self.writingFont = "Helvetica"
        self.fontSize = 18
        self.colorOrange = "color:orange"

        self.mainLayout = QGridLayout()
        self.lblLvl = QLabel(self.msgLvl, self)
        self.lblInfoLvl = QLabel(self.msgInfoLvl, self)
        self.lblExp = QLabel(self.msgExp, self)
        self.lblInfoExp = QLabel(self.msgInfoExp, self)
        self.lblNextLvl = QLabel(self.msgNextLvl, self)
        self.lblInfoNextLvl = QLabel(self.msgInfoNextLvl, self)
        self.lblLastCon = QLabel(self.msgLastCon, self, wordWrap=True)
        self.lblInfoLastCon = QLabel(self.msgInfoLastCon, self)
        self.btnHome = QPushButton(self.msgHome, self)
        self.font = QFont(self.writingFont, self.fontSize)

        self.data = data

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.btnHome.clicked.connect(self.home)

    def home(self):
        self.w = home.Home(self.data)
        self.w.info()
        self.w.show()
        self.hide()

    def fontOnEverything(self):
        self.lblLvl.setFont(self.font)
        self.lblInfoLvl.setFont(self.font)
        self.lblInfoLvl.setStyleSheet(self.colorOrange)
        self.lblExp.setFont(self.font)
        self.lblInfoExp.setFont(self.font)
        self.lblInfoExp.setStyleSheet(self.colorOrange)
        self.lblNextLvl.setFont(self.font)
        self.lblInfoNextLvl.setFont(self.font)
        self.lblInfoNextLvl.setStyleSheet(self.colorOrange)
        self.lblLastCon.setFont(self.font)
        self.lblInfoLastCon.setFont(self.font)
        self.lblInfoLastCon.setStyleSheet(self.colorOrange)
        self.btnHome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblLvl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoLvl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblExp.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoExp.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblNextLvl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoNextLvl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblLastCon.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoLastCon.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblLvl, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoLvl, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.lblExp, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoExp, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.lblNextLvl, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoNextLvl, 3, 1, 1, 1)
        self.mainLayout.addWidget(self.lblLastCon, 4, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoLastCon, 4, 1, 1, 1)