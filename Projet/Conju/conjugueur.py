from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QPushButton, QLabel, QComboBox, QGridLayout, QSizePolicy, QWidget
from PyQt5 import QtGui

import connection_pooling
import home


class Conjugueur(QWidget):
    def __init__(self, userData):
        super().__init__()
        self.title = "Conjugueur"
        self.setWindowTitle(self.title)
        self.font = QtGui.QFont()
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgVerbe = "Verbe"
        self.msgGroupe = "Groupe"
        self.msgAux = "Auxiliaire"
        self.msgTemps = "Temps"
        self.msgSimp = "Simple"
        self.msgComp = "Composé"
        self.msgLTemps = "Liste de temps"
        self.firstPS = "j' / je"
        self.secondPS = "tu"
        self.thirdPS = "il / elle"
        self.firstPP = "nous"
        self.secondPP = "vous"
        self.thirdPP = "ils / elles"
        self.firstPSAns = ""
        self.secondPSAns = ""
        self.thirdPSAns = ""
        self.firstPPAns = ""
        self.secondPPAns = ""
        self.thirdPPAns = ""
        self.msgHome = "Accueil"
        self.writingFont = "Helvetica"
        self.fontSize = 18

        self.mainLayout = QGridLayout()
        self.lblVerbe = QLabel(self.msgVerbe, self)
        self.cbBoxVerbe = QComboBox(self)
        self.lblGroupe = QLabel(self.msgGroupe, self)
        self.lblGrpVerbe = QLabel("", self)
        self.lblAux = QLabel(self.msgAux, self)
        self.lblAuxVerbe = QLabel("", self)
        self.lblTemps = QLabel(self.msgTemps, self)
        self.btnSimple = QPushButton(self.msgSimp, self)
        self.btnComp = QPushButton(self.msgComp, self)
        self.lblLTemps = QLabel(self.msgLTemps, self)
        self.cbBoxTemps = QComboBox(self)
        self.lbl1PS = QLabel(self.firstPS, self)
        self.lbl2PS = QLabel(self.secondPS, self)
        self.lbl3PS = QLabel(self.thirdPS, self)
        self.lbl1PP = QLabel(self.firstPP, self)
        self.lbl2PP = QLabel(self.secondPP, self)
        self.lbl3PP = QLabel(self.thirdPP, self)
        self.lbl1PSConj = QLabel(self.firstPSAns, self)
        self.lbl2PSConj = QLabel(self.secondPSAns, self)
        self.lbl3PSConj = QLabel(self.thirdPSAns, self)
        self.lbl1PPConj = QLabel(self.firstPPAns, self)
        self.lbl2PPConj = QLabel(self.secondPPAns, self)
        self.lbl3PPConj = QLabel(self.thirdPPAns, self)
        self.btnHome = QPushButton(self.msgHome, self)
        self.font = QFont(self.writingFont, self.fontSize)
        self.colorOrange = "color:orange"

        self.connPooling = connection_pooling.Connection_pooling()
        self.cursor = self.connPooling.cursor
        self.verbTimes = ["", 'Présent', 'Imparfait', 'Passé Simple', 'Futur Simple', 'Conditionnel Présent',
                          'Subjonctif Présent',
                          'Subjonctif Imparfait', 'Impératif Présent']
        self.userData = userData
        self.query = "SELECT infinitif FROM verbe"
        self.groupe = "SELECT groupe FROM verbe where infinitif =%s"
        self.auxiliaire = "SELECT auxiliaire FROM verbe where infinitif =%s"
        self.qConj = "conj"
        self.d = {
            0: "Présent", 1: "Imparfait", 2: "Passé Simple", 3: "Futur Simple",
            4: "Conditionnel Présent", 5: "Subjonctif Présent", 6: "Subjonctif Imparfait", 7: "Impératif Présent"}
        self.dmysql = {
            0: "present", 1: "imparfait", 2: "passe_simple", 3: "futur_simple", 4: "conditionnel_present",
            5: "subjonctif_present", 6: "subjonctif_imparfait", 7: "imperatif_present"}
        self.conj = "SELECT * \
                        FROM conj INNER JOIN verbe \
                        ON conj.id = verbe.id WHERE verbe.infinitif=%s"

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.getCurrentListOfVerbAndTimes()
        self.cbBoxTemps.currentTextChanged.connect(self.getCurrentText)
        self.cbBoxVerbe.currentTextChanged.connect(self.getCurrentVerb)

        self.btnHome.clicked.connect(self.home)

    def home(self):
        self.w = home.Home(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def getCurrentListOfVerbAndTimes(self):
        self.cursor.execute(self.query)
        data = self.cursor.fetchall()
        for row in data:
            self.cbBoxVerbe.addItems([str(''.join(row))])
        self.cbBoxTemps.addItems(self.verbTimes)

    def getCurrentVerb(self):
        return self.cbBoxVerbe.currentText()

    def getCurrentText(self):
        self.getTime(self.cbBoxTemps.currentText())
        return True

    def getTime(self, time):
        for k, v in self.d.items():
            if time == v:
                self.getVerb(k)
                return k
        return True

    def getVerb(self, verbeKey):
        for k, v in self.dmysql.items():
            if verbeKey == k:
                self.replaceQueryText(v)
                return v
        return True

    def replaceQueryText(self, verbeValue):
        newQuery = self.conj.replace(self.qConj, verbeValue)
        self.cursor.execute(newQuery, (self.getCurrentVerb(),))
        data = self.cursor.fetchone()
        self.lbl1PSConj.setText(data[2])
        self.lbl2PSConj.setText(data[3])
        self.lbl3PSConj.setText(data[4])
        self.lbl1PPConj.setText(data[5])
        self.lbl2PPConj.setText(data[6])
        self.lbl3PPConj.setText(data[7])

        self.cursor.execute(self.auxiliaire, (self.getCurrentVerb(),))
        newAux = ''.join(self.cursor.fetchone())
        self.lblAuxVerbe.setText(newAux)
        self.cursor.execute(self.groupe, (self.getCurrentVerb(),))
        newGroup = self.cursor.fetchone()
        self.lblGrpVerbe.setText(str(newGroup[0]))

        return True

    def fontOnEverything(self):
        self.lblVerbe.setFont(self.font)
        self.cbBoxVerbe.setFont(self.font)
        self.lblGroupe.setFont(self.font)
        self.lblGrpVerbe.setFont(self.font)
        self.lblGrpVerbe.setStyleSheet(self.colorOrange)
        self.lblAux.setFont(self.font)
        self.lblAuxVerbe.setFont(self.font)
        self.lblAuxVerbe.setStyleSheet(self.colorOrange)
        self.lblTemps.setFont(self.font)
        self.btnSimple.setFont(self.font)
        self.btnComp.setFont(self.font)
        self.lblLTemps.setFont(self.font)
        self.cbBoxTemps.setFont(self.font)
        self.lbl1PS.setFont(self.font)
        self.lbl2PS.setFont(self.font)
        self.lbl3PS.setFont(self.font)
        self.lbl1PP.setFont(self.font)
        self.lbl2PP.setFont(self.font)
        self.lbl3PP.setFont(self.font)
        self.lbl1PSConj.setFont(self.font)
        self.lbl1PSConj.setStyleSheet(self.colorOrange)
        self.lbl2PSConj.setFont(self.font)
        self.lbl2PSConj.setStyleSheet(self.colorOrange)
        self.lbl3PSConj.setFont(self.font)
        self.lbl3PSConj.setStyleSheet(self.colorOrange)
        self.lbl1PPConj.setFont(self.font)
        self.lbl1PPConj.setStyleSheet(self.colorOrange)
        self.lbl2PPConj.setFont(self.font)
        self.lbl2PPConj.setStyleSheet(self.colorOrange)
        self.lbl3PPConj.setFont(self.font)
        self.lbl3PPConj.setStyleSheet(self.colorOrange)
        self.btnHome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.cbBoxVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblGroupe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblGrpVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblAux.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblAuxVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnSimple.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnComp.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblLTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.cbBoxTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl1PS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl2PS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl3PS.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl1PP.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl2PP.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl3PP.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl1PSConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl2PSConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl3PSConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl1PPConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl2PPConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lbl3PPConj.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(100, 100, 100, 100)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblVerbe, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.cbBoxVerbe, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.lblGroupe, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lblGrpVerbe, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.lblAux, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.lblAuxVerbe, 3, 1, 1, 1)
        self.mainLayout.addWidget(self.lblTemps, 1, 2, 1, 1)
        self.mainLayout.addWidget(self.btnSimple, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.btnComp, 3, 2, 1, 1)
        self.mainLayout.addWidget(self.lblLTemps, 1, 3, 1, 1)
        self.mainLayout.addWidget(self.cbBoxTemps, 2, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl1PS, 5, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl2PS, 6, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl3PS, 7, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl1PP, 8, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl2PP, 9, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl3PP, 10, 1, 1, 1)
        self.mainLayout.addWidget(self.lbl1PSConj, 5, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl2PSConj, 6, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl3PSConj, 7, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl1PPConj, 8, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl2PPConj, 9, 3, 1, 1)
        self.mainLayout.addWidget(self.lbl3PPConj, 10, 3, 1, 1)
