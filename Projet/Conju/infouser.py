from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QSizePolicy, QPushButton
import home


class InfoUser(QWidget):
    def __init__(self,data):
        super().__init__()
        self.title = "Informations de l'utilisateur"
        self.setWindowTitle(self.title)
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgUsername = "Nom d'utilisateur"
        self.msgInfoUsername = "Test"
        self.msgFirstName = "Prénom"
        self.msgInfoFirstName = "Test"
        self.msgLastName = "Nom"
        self.msgInfoLastName = "Test"
        self.msgEmail = "Email"
        self.msgInfoEmail = "Test"
        self.msgHome = "Accueil"
        self.writingFont = "Helvetica"
        self.colorOrange = "color:orange"
        self.fontSize = 18

        self.mainLayout = QGridLayout()
        self.lblUsername = QLabel(self.msgUsername, self)
        self.lblInfoUsername = QLabel(self.msgInfoUsername, self)
        self.lblFirstName = QLabel(self.msgFirstName, self)
        self.lblInfoFirstName = QLabel(self.msgInfoFirstName, self)
        self.lblLastName = QLabel(self.msgLastName, self)
        self.lblInfoLastName = QLabel(self.msgInfoLastName, self)
        self.lblEmail = QLabel(self.msgEmail, self)
        self.lblInfoEmail = QLabel(self.msgInfoEmail, self)
        self.btnHome = QPushButton(self.msgHome, self)
        self.font = QFont(self.writingFont, self.fontSize)

        self.data = data

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.showUserInfo()
        self.btnHome.clicked.connect(self.home)

    def home(self):
        self.w = home.Home(self.data)
        self.w.info()
        self.w.show()
        self.hide()

    def showUserInfo(self):
        self.lblInfoUsername.setText(self.data[1])
        self.lblInfoFirstName.setText(self.data[2])
        self.lblInfoLastName.setText(self.data[3])
        self.lblInfoEmail.setText(self.data[4])

    def fontOnEverything(self):
        self.lblUsername.setFont(self.font)
        self.lblInfoUsername.setFont(self.font)
        self.lblInfoUsername.setStyleSheet(self.colorOrange)
        self.lblFirstName.setFont(self.font)
        self.lblInfoFirstName.setFont(self.font)
        self.lblInfoFirstName.setStyleSheet(self.colorOrange)
        self.lblLastName.setFont(self.font)
        self.lblInfoLastName.setFont(self.font)
        self.lblInfoLastName.setStyleSheet(self.colorOrange)
        self.lblEmail.setFont(self.font)
        self.lblInfoEmail.setFont(self.font)
        self.lblInfoEmail.setStyleSheet(self.colorOrange)
        self.btnHome.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.lblUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoUsername.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblFirstName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoFirstName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblLastName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoLastName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblEmail.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblInfoEmail.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(50, 50, 50, 50)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblUsername, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoUsername, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.lblFirstName, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoFirstName, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.lblLastName, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoLastName, 3, 1, 1, 1)
        self.mainLayout.addWidget(self.lblEmail, 4, 0, 1, 1)
        self.mainLayout.addWidget(self.lblInfoEmail, 4, 1, 1, 1)