from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMainWindow, QPushButton, QRadioButton, QLabel, QComboBox, QGridLayout, QSizePolicy, \
    QWidget, QButtonGroup
from PyQt5 import QtGui

import connection_pooling
import exercices
import home


class Choix(QWidget):
    def __init__(self, userData):
        super().__init__()
        self.title = "Choix"
        self.setWindowTitle(self.title)
        self.font = QtGui.QFont()
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)
        self.msgConfirmation = "Confirmation"
        self.msgSubtitle = "Choix nécessaire"
        self.btnWidth = 200
        self.btnHeight = 32
        self.posExWidth = 275
        self.posExHeight = 200
        self.msgChoix = "Choix nécéssaire pour les Exercices"
        self.msgMethPrat = "Méthode de saisie : "
        self.msgTempsModal = "Temps modal : "
        self.msgListeVerbes = "Liste des verbes : "
        self.msgDiff = "Difficulté : "
        self.msgsimpComp = "Simple"
        self.msgGroupe = "Groupe : "
        self.noGr = "2e"
        self.msgCR = "Choix de réponse"
        self.msgClavier = "Au clavier"
        self.msgFac = "Facile"
        self.msgMoy = "Moyen"
        self.msgDif = "Difficile"
        self.msgHome = "Accueil"
        self.font = QFont("Helvetica", 18)
        self.userSentence = 1

        self.mainLayout = QGridLayout()
        self.subtitle = QLabel(self.msgSubtitle, self)
        self.lblMeth = QLabel(self.msgMethPrat, self)
        self.rdbtnCR = QRadioButton(self.msgCR, self)
        self.rdbtnCl = QRadioButton(self.msgClavier, self)
        self.lblTemps = QLabel(self.msgTempsModal, self)
        self.cboxTemps = QComboBox(self)
        self.lblMode = QLabel(self.msgsimpComp, self)
        self.lblVerbe = QLabel(self.msgListeVerbes, self)
        self.cboxVerbe = QComboBox(self)
        self.lblGroupe = QLabel(self.msgGroupe, self)
        self.lblNoGroupe = QLabel(self.noGr, self)
        self.lblDiff = QLabel(self.msgDiff, self)
        self.rdbtnFacile = QRadioButton(self.msgFac, self)
        self.rdbtnMoyen = QRadioButton(self.msgMoy, self)
        self.rdbtnDiff = QRadioButton(self.msgDif, self)
        self.btnHome = QPushButton(self.msgHome, self)
        self.btnConfirmation = QPushButton(self.msgConfirmation, self)


        self.methCB = None
        self.diffCB = None
        self.userData = userData
        self.connPooling = connection_pooling.Connection_pooling()
        self.connexion = self.connPooling.conn
        self.cursor = self.connPooling.cursor
        self.query = "SELECT infinitif FROM verbe"
        self.groupe = "SELECT groupe FROM verbe where infinitif =%s"
        self.userChoice = []

    def info(self):
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.creatingButtonGroup()

        self.cboxTemps.addItems(
            ['Présent', 'Imparfait', 'Passé simple', 'Futur Simple', 'Conditionnel présent', 'Subjonctif Présent',
             'Subjonctif Imparfait', 'Impératif Présent', 'Participe présent'])

        self.cursor.execute(self.query)
        data = self.cursor.fetchall()
        # print("Les verbes contenus dans la base de donnée: ", data)
        for row in data:
            # print(''.join(row))
            self.cboxVerbe.addItems([str(''.join(row))])

        self.cursor.execute(self.groupe, (self.cboxVerbe.currentText(),))
        newGroup = self.cursor.fetchone()
        # print("Groupe: ",newGroup[0])
        self.lblNoGroupe.setText(str(newGroup[0]))

        self.btnConfirmation.clicked.connect(self.enterInfo)
        self.btnConfirmation.clicked.connect(self.exercises)
        # self.lblMeth.text()
        self.btnHome.clicked.connect(self.home)

    def enterInfo(self):
        self.userChoice.append(self.methCB.checkedButton().text())
        self.userChoice.append(self.cboxTemps.currentText())
        self.userChoice.append(self.cboxVerbe.currentText())
        self.userChoice.append(self.diffCB.checkedButton().text())

    def exercises(self):
        self.w = exercices.Exercices(self.userData, self.userChoice, self.userSentence)
        self.w.info()
        self.w.show()
        self.hide()

    def home(self):
        self.w = home.Home(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def creatingButtonGroup(self):
        self.methCB = QButtonGroup()
        self.methCB.addButton(self.rdbtnCl)
        self.methCB.addButton(self.rdbtnCR)
        self.diffCB = QButtonGroup()
        self.diffCB.addButton(self.rdbtnFacile)
        self.diffCB.addButton(self.rdbtnMoyen)
        self.diffCB.addButton(self.rdbtnDiff)

    def fontOnEverything(self):
        self.subtitle.setFont(self.font)
        self.lblMeth.setFont(self.font)
        self.rdbtnCR.setFont(self.font)
        self.rdbtnCl.setFont(self.font)
        self.lblTemps.setFont(self.font)
        self.cboxTemps.setFont(self.font)
        self.lblMode.setFont(self.font)
        self.lblVerbe.setFont(self.font)
        self.cboxVerbe.setFont(self.font)
        self.lblGroupe.setFont(self.font)
        self.lblNoGroupe.setFont(self.font)
        self.lblDiff.setFont(self.font)
        self.rdbtnFacile.setFont(self.font)
        self.rdbtnMoyen.setFont(self.font)
        self.rdbtnDiff.setFont(self.font)
        self.btnHome.setFont(self.font)
        self.btnConfirmation.setFont(self.font)

    def sizePolicyOnEverything(self):
        self.subtitle.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblMeth.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.rdbtnCR.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.rdbtnCl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.cboxTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblMode.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.cboxVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblGroupe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblNoGroupe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblDiff.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.rdbtnFacile.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.rdbtnMoyen.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.rdbtnDiff.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnConfirmation.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(50, 50, 50, 50)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.subtitle, 1, 0, 1, 4)
        self.mainLayout.addWidget(self.lblMeth, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.rdbtnCR, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.rdbtnCl, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.lblTemps, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.cboxTemps, 3, 1, 1, 2)
        self.mainLayout.addWidget(self.lblMode, 3, 3, 1, 1)
        self.mainLayout.addWidget(self.lblVerbe, 4, 0, 1, 1)
        self.mainLayout.addWidget(self.cboxVerbe, 4, 1, 1, 1)
        self.mainLayout.addWidget(self.lblGroupe, 4, 2, 1, 1)
        self.mainLayout.addWidget(self.lblNoGroupe, 4, 3, 1, 1)
        self.mainLayout.addWidget(self.lblDiff, 5, 0, 1, 1)
        self.mainLayout.addWidget(self.rdbtnFacile, 5, 1, 1, 1)
        self.mainLayout.addWidget(self.rdbtnMoyen, 5, 2, 1, 1)
        self.mainLayout.addWidget(self.rdbtnDiff, 5, 3, 1, 1)
        self.mainLayout.addWidget(self.btnConfirmation, 6, 0, 1, 4)