from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QPushButton,  QLabel, QComboBox, QGridLayout, QSizePolicy, \
    QWidget, QCheckBox, QLineEdit, QMessageBox
from PyQt5 import QtGui
import re
import connection_pooling
import home


class Exercices(QWidget):
    def __init__(self, userData, userChoice, userSentence):
        super().__init__()

        self.title = "Exercices"
        self.setWindowTitle(self.title)
        self.font = QtGui.QFont()
        self.top = 100
        self.left = 100
        self.width = 680
        self.height = 500
        self.setGeometry(self.top, self.left, self.width, self.height)

        self.msgTemps = "Temps : "
        self.infoTemps = "TestTemps"
        self.msgIndice = "Indice"
        self.msgVerbe = "Verbe : "
        self.infoVerbe = "TestVerbe"
        self.msgGroupe = "Groupe"
        self.msgDiff = "Difficulté : "
        self.infoDiff = "TestDiff"
        self.msgHome = "Accueil"
        self.msgPersonne = "Personne"
        self.msgNiveau = "Niveau : "
        self.infoNiveau = ""
        self.msgRep = "Réponse"
        self.msgConfirm = "Confirmer"
        self.msgExNext = "Exercice suivant"
        self.msgDisconnect = "Déconnexion"
        self.msgReminding = "Souhaitez-vous vraiment quitter, toutes les informations sauront perdus?"
        self.msgSucc = "Succès"
        self.msgCong = "Bravo, vous avez complété les exercices pour ce verbe"
        self.msgExercice=""
        self.writingFont = "Helvetica"
        self.fontSize = 18
        self.colorOrange = "color:orange"
        self.bgRight = "background-color:green"
        self.bgQuestion = "background-color:skyblue"
        self.bgAnswer = "background-color:salmon"
        self.bgWrong = "background-color:red"
        self.bgAnswerAvailable = "background-color:yellow"
        self.margin = 50

        self.mainLayout = QGridLayout()
        self.lblTemps = QLabel(self.msgTemps, self)
        self.lblTempsEx = QLabel(self.infoTemps, self)
        self.lblIndice = QLabel(self.msgIndice, self)
        self.lblVerbe = QLabel(self.msgVerbe, self)
        self.lblVerbeEx = QLabel(self.infoVerbe, self)
        self.chBoxGroupe = QCheckBox(self.msgGroupe, self)
        self.lblDiff = QLabel(self.msgDiff, self)
        self.lblDiffEx = QLabel(self.infoDiff, self)
        self.chBoxPersonne = QCheckBox(self.msgPersonne, self)
        self.lblNiveau = QLabel(self.msgNiveau, self)
        self.lblNiveauPres = QLabel(self.infoNiveau, self)
        self.btnRep = QPushButton(self.msgRep, self)
        self.lblRepGroupe = QLabel("", self)
        self.lblRepPersonne = QLabel("", self, wordWrap=True)
        self.lblPhrase2 = QLabel(self, wordWrap=True)
        self.lblPhrase1 = QLabel(self, wordWrap=True)
        self.btnHome = QPushButton(self.msgHome, self)
        self.btnConfirm = QPushButton(self.msgConfirm, self)
        self.btnNextEx = QPushButton(self.msgExNext,self)
        self.font = QFont(self.writingFont, self.fontSize)


        self.userData = userData
        self.userChoice = userChoice
        self.userSentence = userSentence
        self.connPooling = connection_pooling.Connection_pooling()
        self.cursor = self.connPooling.cursor
        self.motRecherche = None
        self.msgSplitExercice = self.msgExercice.split()
        self.listeMotsPhrase = None
        self.listeMotTrouve = []
        self.listePetit = []
        self.listeGrand = []
        self.dTimes = {
            0: "Présent", 1: "Imparfait", 2: "Passé Simple", 3: "Futur Simple",
            4: "Conditionnel Présent", 5: "Subjonctif Présent", 6: "Subjonctif Imparfait", 7: "Impératif Présent"}
        self.dmysqlTimes = {
            0: "present", 1: "imparfait", 2: "passe_simple", 3: "futur_simple", 4: "conditionnel_present",
            5: "subjonctif_present", 6: "subjonctif_imparfait", 7: "imperatif_present"}
        self.qConj = "conj"

        self.query = "SELECT phrase0 FROM phrase_conj_facile INNER JOIN verbe  ON phrase_conj_facile.id_verbe = " \
                    "verbe.id INNER JOIN utilisateurs ON phrase_conj_facile.id_utilisateur = utilisateurs.id WHERE " \
                    "verbe.infinitif = %s "
        self.verbeChoisi = self.userChoice[2]
        self.conjQuery = "SELECT conj_0ps FROM conj INNER JOIN verbe ON conj.id = verbe.id WHERE " \
                         "verbe.infinitif=%s; "
        self.groupe = "SELECT groupe FROM verbe where infinitif =%s"
        self.counter = 0
        self.sentenceNb = 1
        self.newConjugateVerb = None
        self.newSentenceChoose = None
        self.newChangeQuery = None

    def info(self):
        self.showMethod()
        self.fontOnEverything()
        self.sizePolicyOnEverything()
        self.positioningWidget()
        self.getTime(self.userChoice[1])
        self.listeMotsPhrase = self.newSentenceChoose.split()
        self.motRecherche = self.newConjugateVerb
        self.make3List()

        # SI CHOIX DE RÉPONSE EST CHOISI, PAR CONTRE ÇA DEVIENT TROP COMPLIQUÉ, J'AI PAS EU LE TEMPS DE FINIR
        #self.addComboItem()

        phrase1 = ' '.join(self.listePetit[0])
        phrase2 = ' '.join(self.listeGrand[0])

        self.lblPhrase1.setText(phrase1)
        self.lblPhrase2.setText(phrase2)
        self.showHelp()
        self.btnConfirm.clicked.connect(self.getValidation)
        self.btnHome.clicked.connect(self.returnHome)


    def getTime(self, time):
        for k, v in self.dTimes.items():
            if time == v:
                self.getTranslateVerb(k)
                return k
        return True

    def getTranslateVerb(self, verbeKey):
        for k, v in self.dmysqlTimes.items():
            if verbeKey == k:
                self.replaceQueryText(v)
                return v
        return True

    def replaceQueryText(self, verbeValue):
        newSentenceQuery = self.query.replace(self.qConj, verbeValue)
        self.cursor.execute(self.changeSentenceQuery(newSentenceQuery), (self.verbeChoisi,))
        data = ''.join(self.cursor.fetchone())
        self.newSentenceChoose = data

        newConjQuery = self.conjQuery.replace(self.qConj, verbeValue)
        self.cursor.execute(self.changeConjWordQuery(newConjQuery), (self.verbeChoisi,))
        data = ''.join(self.cursor.fetchone())
        self.newConjugateVerb = data
        return True

    def changeConjWordQuery(self, querySent):
        if self.userSentence <= 3:
            newQuery = querySent.replace("0ps",str(self.userSentence)+"ps")
        elif self.userSentence >=4 :
            self.minus = 3
            self.pluralState = self.userSentence-self.minus
            newQuery = querySent.replace("0ps", str(self.pluralState) + "pp")
        self.newChangeQuery = newQuery
        return newQuery

    def changeSentenceQuery(self, querySent):
        newQuery = querySent.replace("0", str(self.userSentence))
        return newQuery

    def getValidation(self):
        if self.leMotChoisi.text() == self.listeMotTrouve:
            self.leMotChoisi.setStyleSheet(self.bgRight)
            self.counter=0
            self.btnNextEx.setStyleSheet(self.bgRight)
            self.btnNextEx.clicked.connect(self.exercises)
        else:
            self.counter+=1
            self.leMotChoisi.setStyleSheet(self.bgWrong)
            if self.counter == 3:
                self.btnRep.setStyleSheet(self.bgAnswerAvailable)
                self.btnRep.clicked.connect(self.showAnswer)


    def showHelp(self):
        self.lblVerbeEx.setText(self.userChoice[2])
        self.lblTempsEx.setText(self.userChoice[1])
        self.lblDiffEx.setText(self.userChoice[3])
        self.chBoxPersonne.clicked.connect(self.getCheckBoxPersonClicked)
        self.chBoxGroupe.clicked.connect(self.getCheckBoxGroupClicked)

    def showAnswer(self):
        self.leMotChoisi.setStyleSheet(self.bgAnswer)
        self.leMotChoisi.setText(self.listeMotTrouve)
        self.counter = 0
        self.sentenceNb +=1
        self.btnNextEx.setStyleSheet(self.bgRight)
        self.btnNextEx.clicked.connect(self.exercises)

    def showMethod(self):
        if self.userChoice[0] == "Au clavier":
            self.leMotChoisi = QLineEdit(self)
            self.mainLayout.addWidget(self.leMotChoisi, 9, 0, 1, 5)
        if self.userChoice[0] == "Choix de réponse":
            self.leMotChoisi = QComboBox(self)
            self.mainLayout.addWidget(self.leMotChoisi, 9, 0, 1, 5)


    def getCheckBoxGroupClicked(self):
        if self.chBoxGroupe.isChecked():
            self.cursor.execute(self.groupe, (self.verbeChoisi,))
            groupe = self.cursor.fetchone()
            self.lblRepGroupe.setText(str(groupe[0]))
        else:
            self.lblRepGroupe.clear()
        return self.chBoxGroupe.isChecked()

    def getCheckBoxPersonClicked(self):
        if self.chBoxPersonne.isChecked():
            if self.newChangeQuery.__contains__("1ps"):
                self.lblRepPersonne.setText("1ère / Singulier")
            elif self.newChangeQuery.__contains__("2ps"):
                self.lblRepPersonne.setText("2ème / Singulier")
            elif self.newChangeQuery.__contains__("3ps"):
                self.lblRepPersonne.setText("3ème / Singulier")
            elif self.newChangeQuery.__contains__("1pp"):
                self.lblRepPersonne.setText("1ère / Pluriel")
            elif self.newChangeQuery.__contains__("2pp"):
                self.lblRepPersonne.setText("2ème / Pluriel")
            elif self.newChangeQuery.__contains__("3pp"):
                self.lblRepPersonne.setText("3ème / Pluriel")
        else:
            self.lblRepPersonne.clear()
        return self.chBoxPersonne.isChecked()


    def make3List(self):
        for w in self.listeMotsPhrase:
            MotCherche = None
            if w == self.motRecherche:
                self.listeMotTrouve = w
                MotCherche = self.listeMotsPhrase.index(w)
                self.listePetit.append(self.listeMotsPhrase[:MotCherche])
                self.listeGrand.append(self.listeMotsPhrase[1+MotCherche:])

    def returnHome(self):
        self.quitMessageBox()
        self.returnAnswer = self.msgQuit.exec()
        if self.returnAnswer == QMessageBox.Yes:
            self.home()

    def home(self):
        self.w = home.Home(self.userData)
        self.w.info()
        self.w.show()
        self.hide()

    def exercises(self):
        self.userSentence += 1
        if self.userSentence == 7:
            self.returnMessageBox()
            self.returnAnswer = self.returnMsg.exec()
            if self.returnAnswer == QMessageBox.Ok:
                self.home()
        else:
            self.w = Exercices(self.userData, self.userChoice, self.userSentence)
            self.w.info()
            self.w.show()
            self.hide()

    def quitMessageBox(self):
        self.msgQuit = QMessageBox(self)
        self.msgQuit.setIcon(QMessageBox.Information)
        self.msgQuit.setText(self.msgReminding)
        self.msgQuit.setWindowTitle(self.msgDisconnect)
        self.msgQuit.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        self.msgQuit.show()

    def returnMessageBox(self):
        self.returnMsg = QMessageBox(self)
        self.returnMsg.setIcon(QMessageBox.Information)
        self.returnMsg.setWindowTitle(self.msgSucc)
        self.returnMsg.setText(self.msgCong)
        self.returnMsg.setStandardButtons(QMessageBox.Ok)
        self.returnMsg.show()

    def fontOnEverything(self):
        self.lblTemps.setFont(self.font)
        self.lblTempsEx.setFont(self.font)
        self.lblTempsEx.setStyleSheet(self.colorOrange)
        self.lblIndice.setFont(self.font)
        self.lblVerbe.setFont(self.font)
        self.lblVerbeEx.setFont(self.font)
        self.lblVerbeEx.setStyleSheet(self.colorOrange)
        self.chBoxGroupe.setFont(self.font)
        self.lblDiff.setFont(self.font)
        self.lblDiffEx.setFont(self.font)
        self.lblDiffEx.setStyleSheet(self.colorOrange)
        self.chBoxPersonne.setFont(self.font)
        self.lblNiveau.setFont(self.font)
        self.lblNiveauPres.setFont(self.font)
        self.lblNiveauPres.setStyleSheet(self.colorOrange)
        self.btnRep.setFont(self.font)
        self.btnHome.setFont(self.font)
        self.btnConfirm.setFont(self.font)
        self.btnNextEx.setFont(self.font)
        self.lblRepGroupe.setFont(self.font)
        self.lblRepGroupe.setStyleSheet(self.colorOrange)
        self.lblRepPersonne.setFont(self.font)
        self.lblRepPersonne.setStyleSheet(self.colorOrange)
        self.leMotChoisi.setFont(self.font)
        self.lblPhrase1.setFont(self.font)
        self.lblPhrase2.setFont(self.font)
        self.lblPhrase1.setStyleSheet(self.bgQuestion)
        self.lblPhrase2.setStyleSheet(self.bgQuestion)

    def sizePolicyOnEverything(self):
        self.lblTemps.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblTempsEx.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblIndice.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblVerbeEx.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.chBoxGroupe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblDiff.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblDiffEx.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.chBoxPersonne.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblNiveau.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblNiveauPres.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnRep.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnHome.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnConfirm.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.btnNextEx.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblRepGroupe.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.lblRepPersonne.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

    def positioningWidget(self):
        self.mainLayout.setContentsMargins(self.margin, self.margin, self.margin, self.margin)
        self.setLayout(self.mainLayout)
        self.mainLayout.addWidget(self.btnHome, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.lblTemps, 1, 0, 1, 1)
        self.mainLayout.addWidget(self.lblTempsEx, 1, 1, 1, 1)
        self.mainLayout.addWidget(self.lblVerbe, 2, 0, 1, 1)
        self.mainLayout.addWidget(self.lblVerbeEx, 2, 1, 1, 1)
        self.mainLayout.addWidget(self.lblDiff, 1, 2, 1, 1)
        self.mainLayout.addWidget(self.lblDiffEx, 1, 3, 1, 1)
        self.mainLayout.addWidget(self.lblNiveau, 2, 2, 1, 1)
        self.mainLayout.addWidget(self.lblNiveauPres, 2, 3, 1, 1)
        self.mainLayout.addWidget(self.lblIndice, 3, 0, 1, 1)
        self.mainLayout.addWidget(self.chBoxGroupe, 3, 1, 1, 1)
        self.mainLayout.addWidget(self.chBoxPersonne, 3, 2, 1, 1)
        self.mainLayout.addWidget(self.btnRep, 3, 3, 1, 1)
        self.mainLayout.addWidget(self.lblRepGroupe, 4, 1, 1, 1)
        self.mainLayout.addWidget(self.lblRepPersonne, 4, 2, 1, 1)
        self.mainLayout.addWidget(self.btnConfirm, 15, 0, 1, 1)
        self.mainLayout.addWidget(self.btnNextEx, 15, 2, 1, 1)
        self.mainLayout.addWidget(self.lblPhrase1, 6, 0, 3, 5)
        self.mainLayout.addWidget(self.lblPhrase2, 10, 0, 3, 5)

    #VOICI LE PROTOTYPE DE LA FONCTION CHOIX DE RÉPONSE
    # def addComboItem(self):
    #     self.leMotChoisi.addItem(self.listeMotTrouve)
    #     lastChar = self.listeMotTrouve[-1:]
    #     wordChoosen = self.listeMotTrouve[:-1]
    #     FirstChoiceChar = str(lastChar).replace(lastChar, "s")
    #     SecondChoiceChar = str(lastChar).replace(lastChar, "t")
    #     ThirdChoiceChar = str(lastChar).replace(lastChar, "d")
    #     FourthChoiceChar = str(lastChar).replace(lastChar, "x")
    #     FirstChoice = wordChoosen + FirstChoiceChar
    #     SecondChoice = wordChoosen + SecondChoiceChar
    #     ThirdChoice = wordChoosen + ThirdChoiceChar
    #     FourthChoice = wordChoosen + FourthChoiceChar
    #     self.leMotChoisi.addItems([FirstChoice,SecondChoice,ThirdChoice,FourthChoice])